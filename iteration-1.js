//1.1
const $$button = document.querySelector('#btnToClick');

$$button.addEventListener('click',clicar);

function clicar(event){
    console.log(event);
}

//1.2
const $$focus = document.querySelector('.focus');
$$focus.addEventListener('focus',inputFocus);

function inputFocus(event){
    let text = event.target.value;
    console.log(text);
}

//1.3
const $$input = document.querySelector('.value');
$$input.addEventListener('input', textoForm);

function textoForm(event){
    let text = event.target.value;
    console.log(text);
}